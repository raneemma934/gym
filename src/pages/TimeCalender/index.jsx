import { Button, Paper, TextField } from '@mui/material';
import React from 'react';
import { useForm, Controller, useFieldArray } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { setTimeCoach } from '../coach/store';

function MyForm({ Data }) {
  console.log("🚀 ~ MyForm ~ Data:", Data)
  const { control, handleSubmit } = useForm();
  const dispatch = useDispatch()

  const { fields, append, remove } = useFieldArray({
    control,
    name: "times",
  });

  const onSubmit = (data) => {
    const formData = new FormData();
    data.times.forEach((timeSlot, index) => {
      formData.append(`coachTime[${index}][startTime]`, timeSlot.starttime);
      formData.append(`coachTime[${index}][endTime]`, timeSlot.endtime);
      formData.append(`coachTime[${index}][dayId]`, timeSlot.dayId);
    });
    formData.append('coachId', Data[0].id)
    dispatch(setTimeCoach(formData));
    console.log(data);

  };

  return (
    <Paper>

      <form onSubmit={handleSubmit(onSubmit)}>
        {fields.map((field, index) => (
          <div key={field.id}>
            <Controller
              name={`times.${index}.starttime`}
              control={control}
              label="Start time"
              render={({ field }) => (
                <TextField
                  {...field}

                  type='datetime-local'
                  sx={{ margin: 10 }}
                  variant="outlined"
                />
              )}
            />
            <Controller
              name={`times.${index}.endtime`}
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  label="End time"
                  type='datetime-local' // Changed to 'datetime-local' for datetime input
                  sx={{ margin: 10 }}
                  variant="outlined"
                />
              )}
            />
            <Controller
              name={`times.${index}.dayId`}
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  label="Day ID"
                  sx={{ margin: 10 }}
                  variant="outlined"
                />
              )}
            />
            <Button type="button" onClick={() => remove(index)} sx={{ margin: 10 }}>
              Remove
            </Button>
          </div>
        ))}
        <Button type="button" onClick={() => append({})} sx={{ margin: 10 }}>
          Add Time
        </Button>
        <Button type="submit" variant="contained" color="primary" sx={{ margin: 10 }}>
          Submit
        </Button>
      </form>
    </Paper>
  );
}

export default MyForm;
