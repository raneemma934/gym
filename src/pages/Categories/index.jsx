import { Paper, Typography, TextField, Button, Input, MenuItem, Select, OutlinedInput } from '@mui/material';
import React, { useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { setCategory } from '../coach/store';

function getStyles(name, personName, theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}


export default function Categories() {
  const { control, handleSubmit, reset } = useForm();
  const { t } = useTranslation()
  const dispatch = useDispatch()
  const [image, setImage] = useState(null);
  const [file, setfile] = useState(null);

  const handleImageChange = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    setfile(file)
    reader.onload = () => {
      setImage(reader.result);
    };

    if (file) {
      reader.readAsDataURL(file);
    }
  };

  const onSubmit = (data) => {
    const formData = new FormData()
    formData.append('name', data.name)
    formData.append('imageUrl', file)
    formData.append('description', data.description)
    formData.append('type', data.contentType)

    console.log(data);
    dispatch(setCategory(formData))

    // You can perform further actions with the submitted data here
    reset(); // Reset the form after submission
  };

  return (
    <Paper sx={{ padding: 20 }}>
      <Typography variant="h5">{t('AddCategory')}</Typography>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div>
          <input type="file" onChange={handleImageChange} />
          {image && (
            <div>
              <h2>Preview:</h2>
              <img src={image} alt="Uploaded" style={{ maxWidth: '100%' }} />
            </div>
          )}
        </div>
        <Controller
          name="name"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <TextField
              {...field}
              fullWidth
              margin="normal"
              label={t("Name")}
            />
          )}
        />
        <Typography sx={{my:"10px"}}>{t("Type")}</Typography>
        <Controller
          name="contentType"
          control={control}
          defaultValue="Type"
          render={({ field }) => (
            <Select
              {...field}
              fullWidth
          defaultValue="Type"
                margin="normal"
            >
              <MenuItem  value="food">{t("Food")}</MenuItem>
              <MenuItem  value="sport">{t("Sport")}</MenuItem>
            </Select>
          )}
        />
        <Controller
          name="description"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <TextField
              {...field}
              fullWidth
              margin="normal"
              label={t("Description")}
              multiline
              rows={4}
            />
          )}
        />

        <Button type="submit" variant="contained" color="primary">{t('Submit')}</Button>
      </form>
    </Paper>
  );
}
