
export const SubscriptionsData = elements => {

  return elements.map(element => {
    const dateOnly = element?.SubscriptionDate ? element.SubscriptionDate.split('T')[0] : null;

    return {
      id: element?.id,
      name: element?.userName,
      remainingTime: element?.remainingTime,
      paidStatus: element?.paidStatus,
      daysNotPaid: element?.daysNotPaid,
      SubscriptionDate: dateOnly
    }
  })
}
