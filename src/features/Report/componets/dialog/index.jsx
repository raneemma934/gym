import React, { useState } from "react";
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from "@mui/material";
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import { useDispatch } from "react-redux";
import { RemoveReport, getReportsData } from "src/pages/report/store";
import { useTranslation } from "react-i18next";

export default function AlertDialog({ id, open, handleClose, handleDelete }) {
  const [localId, setLocalId] = useState(id);
  const dispatch = useDispatch()
  const { t } = useTranslation()

  const handleDeleteAPI = () => {
    dispatch(RemoveReport(localId))
    dispatch(getReportsData())
    handleClose()
  };

  return (

    <React.Fragment>
      <Dialog
        onClose={handleClose}
        open={open}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle style={{ fontSize: "19px", color: '#B4B4B3' }}>
          {t('deletin')}
        </DialogTitle>

        <DialogActions style={{ display: 'flex', justifyContent: 'center', padding: '10px' }}>
          <Button onClick={handleClose} style={{ color: '#B4B4B3' }}>{t('Cancel')}</Button>
          <Button onClick={() => handleDeleteAPI()} autoFocus>
            {t('Delete')}
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}
