// ** React Imports
import { useEffect, useState } from 'react'

// ** MUI Imports
import Tab from '@mui/material/Tab'
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'
import Avatar from '@mui/material/Avatar'
import TabContext from '@mui/lab/TabContext'
import CardHeader from '@mui/material/CardHeader'
import Typography from '@mui/material/Typography'
import CardContent from '@mui/material/CardContent'
import { useTheme } from '@mui/material/styles'

// ** Custom Components Import
import Icon from 'src/@core/components/icon'
import OptionsMenu from 'src/@core/components/option-menu'
import CustomAvatar from 'src/@core/components/mui/avatar'
import ReactApexcharts from 'src/@core/components/react-apexcharts'

// ** Util Import
import { hexToRGBA } from 'src/@core/utils/hex-to-rgba'
import { Stack } from '@mui/system'
import { useTranslation } from 'react-i18next'



const PlayerFinance = (Data) => {
  console.log("🚀 ~ PlayerFinance ~ Data:", Data)

  // ** State
  // console.log("finannnnnnnnnnn",Data?.Data?.data[0].monthName)
  const [data, setData] = useState()
  useEffect(() => {
    setData(Data)
  }, [])
  let Month = [Data?.Data[2]?.finance, Data?.Data[1]?.finance, Data?.Data[0]?.finance]

  const tabData = [
    {
      type: 'orders',
      avatarIcon: 'tabler:shopping-cart',
      series: [{ data: [...Month] }]
    },
  ]


  const renderTabPanels = (value, theme, options, colors, data) => {
    console.log("🚀 ~ renderTabPanels ~ data:", data)


    return tabData.map((item, index) => {
      console.log("🚀 ~ returntabData.map ~ item:", item)
      const max = Math.max(...item.series[0].data)
      const seriesIndex = item.series[0].data.indexOf(max)
      const finalColors = colors.map((color, i) => (seriesIndex === i ? hexToRGBA(theme.palette.primary.main, 1) : color))

      return (
        <TabPanel key={index} value={item.type}>
          {data ? <ReactApexcharts type='bar' height={263} options={{ ...options, colors: finalColors }} series={item.series} /> : null}
        </TabPanel>
      )
    })
  }

  const [value, setValue] = useState('orders')
  const { t } = useTranslation()

  // ** Hook
  const theme = useTheme()

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }
  const colors = Array(9).fill(hexToRGBA(theme.palette.primary.main, 0.16))

  const options = {
    chart: {
      parentHeightOffset: 0,
      toolbar: { show: false }
    },
    plotOptions: {
      bar: {
        borderRadius: 6,
        distributed: true,
        columnWidth: '35%',
        startingShape: 'rounded',
        dataLabels: { position: 'top' }
      }
    },
    legend: { show: false },
    tooltip: { enabled: false },
    dataLabels: {
      offsetY: -15,
      formatter: val => `${val}k`,
      style: {
        fontWeight: 500,
        colors: [theme.palette.text.secondary],
        fontSize: theme.typography.body1.fontSize
      }
    },
    colors,
    states: {
      hover: {
        filter: { type: 'none' }
      },
      active: {
        filter: { type: 'none' }
      }
    },
    grid: {
      show: false,
      padding: {
        top: 20,
        left: -5,
        right: -8,
        bottom: -12
      }
    },
    xaxis: {
      axisTicks: { show: true },
      axisBorder: { color: theme.palette.divider },
      categories: [
        `${Data?.Data?.[2]?.monthName}`,
        `${Data?.Data?.[1]?.monthName}`,
        `${Data?.Data?.[0]?.monthName}`,
      ],
      labels: {
        style: {
          colors: theme.palette.text.disabled,
          fontFamily: theme.typography.fontFamily,
          fontSize: theme.typography.body2.fontSize
        }
      }
    },
    yaxis: {
      labels: {
        offsetX: -15,
        formatter: val => `$${val}k`,
        style: {
          colors: theme.palette.text.disabled,
          fontFamily: theme.typography.fontFamily,
          fontSize: theme.typography.body2.fontSize
        }
      }
    },

  }

  return (
    <Card>
      <CardHeader
        title={t('EarningReports')}



      />
      <CardContent sx={{ '& .MuiTabPanel-root': { p: 0 } }}>
        <TabContext value={value}>
          <TabList
            variant='scrollable'
            scrollButtons='auto'
            onChange={handleChange}
            aria-label='earning report tabs'
            sx={{
              border: '0 !important',
              '& .MuiTabs-indicator': { display: 'none' },
              '& .MuiTab-root': { p: 0, minWidth: 0, borderRadius: '10px', '&:not(:last-child)': { mr: 4 } }
            }}
          >
          </TabList>
          {Data && renderTabPanels(value, theme, options, colors, data?.Data)}
        </TabContext>
      </CardContent>
    </Card>
  )
}

export default PlayerFinance
